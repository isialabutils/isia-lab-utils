#Il faut juste faire une chose avant, aller dans les paramètre de votre 
#google account, activer l'accès aux applications moins sécurisées =>
#https://myaccount.google.com/lesssecureapps

# This is a code snippet that can be interesting for sending emails 
# at the end of a training for example.
# You can send the results and saved figures for example
# If you use Keras, you can send the history (acc, loss... etc.)
# And if you save plots on your disc, you can attach them too using the 
# yagmail.inlint('path.png')

import yagmail
yag = yagmail.SMTP("GmailUsername", "Password")

# Or without specifying the password and you enter it privetly
# yag = yagmail.SMTP("GmailUsername")
#
contents = ['Hello,\n', 
            '\nTraining has ended successfully! Here are the results:', 
            str(model_history.history), 
            yagmail.inline("acc.png"), 
            yagmail.inline("loss.png"),
            yagmail.inline("mse.png")] 
yag.send('receiver@mail.com', 'Title', contents)