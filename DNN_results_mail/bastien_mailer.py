## Works with config.yml file.

import smtplib
import yaml

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

def send_mail(subject, message, to_list=None):
    with open('config.yml', 'r') as file:
        configuration = yaml.load(file, Loader=yaml.FullLoader)['mail']
        msg = MIMEMultipart()

        msg['From'] = configuration['from']
        to = configuration['to'] if to_list == None else to_list
        msg['To'] = ', '.join(to)

        msg['Subject'] = subject

        msg.attach(MIMEText(message))

        mailserver = smtplib.SMTP('smtp.gmail.com', 587)
        mailserver.ehlo()
        mailserver.starttls()
        mailserver.ehlo()
        mailserver.login(configuration['from'], configuration['pass'])
        mailserver.sendmail(msg['From'], to, msg.as_string())
        mailserver.quit()


def main() :
    
    send_mail("Test", "Un petit message d'essai")