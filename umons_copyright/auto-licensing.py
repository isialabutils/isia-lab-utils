
import argparse
import os


def main():
    parser = argparse.ArgumentParser(description="Insert a header in all files.")
    parser.add_argument("header_file", metavar= "HEADER_FILE", help="Header content to insert.")
    parser.add_argument("path", metavar= "FILE_PATH", help="Path to browse for files and add header to.")

    parser.add_argument("-c", "--comment_tag", type=str, default='#',
                        help="Comment tag to use before each line of header. By default, '#' is used.")

    parser.add_argument("--ext", type=str, nargs='+', default=None,
                        help="""Files extension accepted. By default, all are accepted. 
                        You can blacklist these extensions by using -b flag""")

    parser.add_argument("-b", action='store_false', default=True,
                        help="If this flag is set, defined extensions are blacklisted rather than whitelisted.")

    parser.add_argument("--excl_dirs", type=str, nargs='+', default=None,
                        help="""Directories that should not be browse to add header.""")

    parser.add_argument("-f", action='store_true', default=False,
                        help="Force adding header in found files even if header is detected.")

    parser.add_argument("--force_erase", action='store_true', default=False,
                        help="Force erasing header in found files before writing header")

    args = parser.parse_args()

    header = load_header(args.header_file, args.comment_tag)

    files = scan_directory(args.path, args.excl_dirs, args.ext, args.b)
    for file in files:
        insert_header(header, file, args.comment_tag, args.f, args.force_erase)

    print("Header inserted in {} file(s).".format(len(files)))


def load_header(header_file,  comment_tag='#'):
    header_content = ''
    with open(header_file, 'r') as f:
        for line in f:
            header_content += comment_tag + ' ' + line

    header_content.rstrip()

    return header_content + '\n\n'


def scan_directory(dir_path, exclude_dir, ext_list, is_whitelist=True):
    files = []
    for file in os.listdir(dir_path):
        if exclude_dir is None or file not in exclude_dir:
            path = os.path.join(dir_path, file)
            if os.path.isdir(path):
                files += scan_directory(path, exclude_dir, ext_list, is_whitelist)
            elif check_ext(file, ext_list, is_whitelist):
                files += [path]
    return files


def insert_header(header_content, file, comment_tag, force=False, force_erase=False):
    # TODO Avoid loading all the file?

    if not force and not check_header(file, comment_tag, force_erase):
        return

    with open(file, 'r+') as f:
        file_content = f.read()
        f.seek(0)
        f.write(header_content + file_content)


def check_ext(file, ext_list, is_whitelist=True):
    if ext_list is None:
        return True

    if os.path.splitext(file)[1] in ext_list:
        return is_whitelist
    else:
        return not is_whitelist


def find_header(file_path, comment_tag):
    buffer = ''
    with open(file_path, 'r') as f:
        line = f.readline()
        while line is not None:
            if len(line.strip()) != 0 and not line.startswith(comment_tag):
                break
            else:
                buffer += line
                seek_content = f.tell()

            line = f.readline()

    if len(buffer.strip()) == 0:
        return None
    else:
        return buffer, seek_content


def check_header(file_path, comment_tag, force_erase=False):
    res = find_header(file_path, comment_tag)

    if res is None:
        return True

    print(">>> ! Header found in file " + file_path + ": ")
    print(res[0].strip())

    if force_erase:
        erase_header(file_path, res[1])
        return True

    agree = input("\nDo you want to insert your own header before? [Y/n]")
    if agree == 'Y':
        return True

    agree = input("\nDo you want to erase this header with yours? [Y/n]")
    if agree == 'Y':
        erase_header(file_path, res[1])
        return True

    return False


def erase_header(file_path, seek_content):
    with open(file_path, 'r') as f:
        f.seek(seek_content)
        buffer = f.read()

    print(buffer)

    with open(file_path, 'w') as f:
        f.write(buffer)


if __name__ == '__main__' :
    main()

