# isia-lab-utils

This repository gathers functions, classes adn code snippet that can be often use by everyone @ISIA Lab.

### Repository content ###

- API_wrapper
    * Contains code to extract data through differrent websites API.
- DNN_results_mail
    * Send results of DNN training through mail.
- file
    * Common reading and writing file operations all in one place
- sound_to_img
    * Scripts that convert sound features to images in order to be used in CNN trainings.
- umons_copyright
    * Files to automatically add umons licensing copyright text in the header of each file.