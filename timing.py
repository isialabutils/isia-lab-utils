import json

class Timing :

    def __init__( self, h, m, s, ms ) :
        self.hour = h
        self.min = m
        self.sec = s
        self.ms = ms
    
    def update( self, data, base ) :
        '''
        Check whether a value (data) is above its base. If so, extract the real and next amounts.
        TODO : need clearer explanation
        '''
        if( data > base ) :
            next = data // base
            amount = data % base
        elif( data < 0 ) :
            amount = base + data
            next = 1
        else :
            amount = data
            next = 0
        
        return amount, next
    
    def __add__ ( self, t ) :
        '''
        Overload + operator. Enables to add two Timing objects together.
        '''
        result = Timing( 0, 0, 0, 0 )
        result.ms = self.ms + t.ms
        result.sec = self.sec + t.sec
        result.min = self.min + t.min
        result.hour = self.hour + t.hour
        result.ms, next_sec = self.update( result.ms, 1000 )
        result.sec += next_sec
        result.sec, next_min = self.update( result.sec, 60 )
        result.min += next_min
        result.min, next_hour = self.update( result.min, 60 )
        result.hour += next_hour

        return result

    def __sub__ ( self, t ) :
        '''
        Overload - operator. Enables to subtract one Timing object to another.
        '''
        result = Timing( 0, 0, 0, 0 )
        result.ms = self.ms - t.ms
        result.sec = self.sec - t.sec
        result.min = self.min - t.min
        result.hour = self.hour - t.hour
        result.ms, next_sec = self.update( result.ms, 1000 )
        result.sec -= next_sec
        result.sec, next_min = self.update( result.sec, 60 )
        result.min -= next_min
        result.min, next_hour = self.update( result.min, 60 )
        result.hour -= next_hour

        return result
    
    def __eq__( self, t ) :
        '''
        Check if two timings are the same.
        '''
        if( self.hour == t.hour and self.min == t.min and self.sec == t.sec and self.ms == t.ms ) :
            return True
        else : 
            return False

    def asString( self ) :
        '''
        Convert timing as a string of type h::min:sec.ms. Useful to be used as a value for parameters of ffmpeg for example.
        Return : 
            t_as_str : the timing as a string of type h::min:sec.ms
        '''
        t_as_str = str( self.hour ) + ':' + str( self.min ) + ':' + str( self.sec ) + '.' + str( self.ms )
        return t_as_str


    @staticmethod
    def list2Timing ( list ) :
        '''
        Convert a list of 4 integers (hours, minutes, sec, ms) into a timing object
        Argument :
            list : list of 4 integers corresponding to the amount of hours, minutes, seconds and ms
        Return :
            timing : return the corresponding Timing object
        '''
        if( len( list ) == 4 ) :
            t = Timing( list[ 0 ], list[ 1 ], list[ 2 ], list[ 3 ] )
            return t    

    @staticmethod
    def convertMinSecImgSsi2HoursMinSecMs( timing ) :
        '''
        Convert timing in Minute-seconde-IMG(base24)-ssi(base80) format into H Min Sec Ms format
        Argument :
            timing : timing in Minute- seconde-IMG (base24)-ssi (base80). Can be either a string where is element is separated by a '-' or a list of 4 integers
        Return :
            timing_hminsecms : conversion of the input argument as a Timing object
        '''

        timing_hminsecms = Timing( 0, 0, 0, 0 )
        img_duration = 1. / 24.
        ssi_duration = img_duration / 80.
        
        ## if timing is a string in form H-min-sec-ms
        if( type( timing ) is str ) :
            timing = timing.split( '-' )
            for i in range( len( timing ) ) :
                timing[ i ] = int( timing[ i ] )

        #timing_minsecimgssi = Timing.list2Timing( timing )

        timing_hminsecms.min = timing[ 0 ] ## first element of MinSecImgSsi format 
        timing_hminsecms.sec = timing[ 1 ]
        timing_hminsecms.ms = int( ( ( int( timing[ 2 ] ) * img_duration + int( timing[ 3 ] ) * ssi_duration ) % 1 ) * 1000 )

        return timing_hminsecms

    @staticmethod
    def convertMinSecImgSsi2Sec( timing ) :
        '''
        Convert timing in Minute- seconde-IMG (base24)-ssi (base80) format into ms
        Argument :
            timing : timing in Minute- seconde-IMG (base24)-ssi (base80)
        Return :
            timing_ms : timing in ms
        '''
        timing_s = 0.
        img_duration = 1. / 24.
        ssi_duration = img_duration / 80.
        img_ssi_to_s = ( timing[ 2 ] * img_duration + timing[ 3 ] * ssi_duration )
        
        timing_s = round( ( timing[ 0 ] * 60. + timing[ 1 ] + img_ssi_to_s ), 2 )

        #print( timing, ' - ', timing_ms )

        return timing_s

class TimingEncoder( json.JSONEncoder ) :
    '''
    To test
    t = Timing( 5, 6, 7, 890)
    print( TimingEncoder().encode( t ) )
    '''
    def default( self, object ) :
        if( isinstance( object, Timing ) ) :
            return object.__dict__
        else :
            return json.JSONEncoder.default( self, object )