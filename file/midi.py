from midiutil import MIDIFile


def createMIDI( pitch, time, duration, tempo, out ):
    '''
    Create MIDI file bases on lists of pitches, times and durations. 
    For the moment, only works for one instrument (i.e, one track) and don't work with tempo variation.
    Argument :
        pitch : list of pitches
        time : list of time intervals
        duration : list of events duration
        tempo : tempo value of the music piece
        out : name of the MIDI file to be saved
    Return :
        out : ??
    '''
    f = MIDIFile( 1 )  # One track, defaults to format 1 (tempo track is created
                    # automatically)
    track    = 0
    channel  = 0
    volume = 100

    f.addTempo( track, 0, tempo )

    print( 'Len of pitch ', len( pitch ), ' len of time ', len( time ), ' len duration ', len( duration ) )
    for i, p in enumerate( pitch ):
        f.addNote( track, channel, int( p ), time[ i ] , duration[ i ], volume )

    with open( out, "wb" ) as output_file:
        f.writeFile( output_file )
    
    return out