'''
This file generate pictures from CQT spectral representation of audiofiles.
The script expects to see a dataset fodler with an audio folder in it and will create the images folder.

To execute :
    * python export_cqt_images.py /main/dataset/folder
    * python export_cqt_images.py ../../newGAC-tmp/Datasets/midi/ && python export_cqt_images.py ../../newGAC-tmp/Datasets/string/


Date of creation : 29/05/2019
Author : Loic Reboursiere | loicreboursiere@gmail.com
'''

import os, sys, shutil
import random
import librosa, librosa.display
import numpy as np
import matplotlib.pyplot as plt
from tqdm import trange

def export_cqt_images( audiopath, savepath = 'cqt-export.png', duration = 0., hop_length = 256, n_bins = 192, bins_per_octave = 48, fmin = 73.  ) :
    '''
    Compute CQT from librosa and save png images of librosa.display.specshow without any borders.
    Arguments :
        audiopath : path of the audiofile to compute cqt on
        savepath : path to save the outputted image
        duration : duration of the sound to analyse (starting at 0). If duration is 0., the whole sound is analysed.
        hop_length : size of the hop length of the analysis
        n_bins : number of bins on which the frequency scale will be coded
        bins_per_octave : number of bins on which an octave spreads
        fmin : minimum frequency to track
    '''

    y, sr = librosa.load( audiopath )

    if( duration == 0. ) :
        C = np.abs( librosa.cqt( y, sr = sr, hop_length = hop_length, n_bins = n_bins, bins_per_octave = bins_per_octave, fmin = fmin ) )
    else :
        C = np.abs( librosa.cqt( y[ 0 : int( duration * sr ) ], sr = sr, hop_length = hop_length, n_bins = n_bins, bins_per_octave = bins_per_octave, fmin = fmin ) )

    fig = plt.figure( frameon = False )
    ax = plt.Axes(fig, [0., 0., 1., 1.])
    ax.set_axis_off()
    fig.add_axes(ax)
    librosa.display.specshow( librosa.amplitude_to_db( C, ref=np.max) )
    fig.savefig( savepath )

    plt.savefig( savepath )
    plt.close()

def buildSets( img_files, output_folder, train_data_percentage ) :

    nb_of_files = len( img_files )
    nb_of_train_files = int( nb_of_files * train_data_percentage )

    print( '--- Nb of files, total : ', str( nb_of_files ), ' train : ', str( nb_of_files - nb_of_train_files ), ' val : ', str( nb_of_train_files ) )

    train_folder = os.path.join( output_folder, 'train')
    val_folder = os.path.join( output_folder, 'valid' )
    if( not os.path.isdir( train_folder ) ) :
        os.mkdir( train_folder )
    if( not os.path.isdir( val_folder ) ) :
        os.mkdir( val_folder )

    idx_of_train_files = random.sample( list( np.arange( nb_of_files ) ), nb_of_train_files )

    print( 'Files copied in valid folder :')
    for nb, i in enumerate( idx_of_train_files ) :
        dest = img_files[ i ].replace( '/images/', '/valid/' )
        dest = dest.replace( 'CQT/', '' )
        if( not os.path.isdir( os.path.dirname( dest ) ) ) :
            os.makedirs( os.path.dirname( dest ) )
        shutil.copyfile( img_files[ i ], dest )
        #print( '*** ', str( nb ), ' - ', img_files [ i ] )

    for idx in sorted( idx_of_train_files, reverse = True ) :
        #print( 'removing index : ', idx )
        del img_files[ idx ]

    print( 'Files copied in train folder :')
    for nb, f in enumerate( img_files ) :
        dest = f.replace( '/images/', '/train/' )
        dest = dest.replace( 'CQT/', '' )
        if( not os.path.isdir( os.path.dirname( dest ) ) ) :
            os.makedirs( os.path.dirname( dest ) )
        shutil.copyfile( f, dest )
        #print( '*** ', str( nb ), ' - ', f )


def getAudioFilesPath( datasetpath ) :
    '''
    Returns the list of all the classes audio files fullpath.
    Look for audiofile in datasetpath + 'audio/'.
    '''

    audio_files = []
    audio_folder = os.path.join( datasetpath, 'audio' )

    classes = os.listdir( audio_folder )
    for classe in classes :
        classe_path = os.path.join( audio_folder, classe )
        files = os.listdir( classe_path )
        for f in files :
            if( f.endswith( '.wav' ) ) :
                audio_files.append( os.path.join( classe_path, f ) )

    return audio_files

### for debug
def rmCQTFromAudio( dataset_audio_folder ) :
    '''
    A bug was creating CFP folders in classes of the audio folders.
    This function removes those folders.
    '''

    classes = os.listdir( dataset_audio_folder )
    for classe in classes :
        classe_path = os.path.join( dataset_audio_folder, classe )
        cqt_path = os.path.join( classe_path, 'CQT' )
        if( os.path.isdir( cqt_path ) ) :
            shutil.rmtree( cqt_path )

## Check the number of parameters for the function ##
# def checkParam( nb ) :
def checkParam( nbMin, nbMax ) :
    # if( len(sys.argv ) != nb+1 ) :
    if( len(sys.argv ) < nbMin+1 or len(sys.argv) > nbMax+1 ) :
        print( '### Wrong syntax, must be : ' )
        scriptSyntax()
        return False
    else :
        return True

## Syntax of the program, show if no good amount of parameters ##
def scriptSyntax() :
    print( 'python ' + sys.argv[ 0 ] + ' /path/to/datasets/folder duration' )


def main():

    # if( checkParam( 1 ) ) :
    if( checkParam( 1, 2) ) :
        img_savepath = []
        datasetpath = sys.argv[ 1 ]
        if( not os.path.isdir( os.path.join( datasetpath, 'images') ) ) :
            os.makedirs( os.path.join( datasetpath, 'images') )
        audio_files = getAudioFilesPath( datasetpath )
        img_savepath = [ a.replace( '/audio/', '/images/') for a in audio_files ]
        img_savepath = [ a.replace( '.wav', '.png') for a in img_savepath ]
        img_savepath = [ os.path.join( os.path.dirname( f ), 'CQT', os.path.basename( f ) ) for f in img_savepath ]

        for f in img_savepath :
            if( not os.path.isdir( os.path.dirname( f ) ) ) :
                os.makedirs( os.path.dirname( f ) )

        for idx, a in enumerate( audio_files ) :
#            if( idx < 3 ) :
            if( os.path.isdir( os.path.dirname( img_savepath[ idx ] ) ) ) :
                if( not os.path.isfile(img_savepath[ idx ] ) ) :
                    duration = 0.
                    if len(sys.argv) > 2:
                        duration = int(sys.argv[2])
                    export_cqt_images( a, savepath = img_savepath[ idx ], duration=duration )
                    # export_cqt_images( a, savepath = img_savepath[ idx ] )
                    print( idx, '/', len( audio_files ), ' - ',  img_savepath[ idx ] )
                else :
                    print( 'File ', img_savepath[ idx ], ' already exists.')
            else :
                print( 'Trying to export to an unknown folder ', img_savepath[ idx ] )


        buildSets( img_savepath, datasetpath, 0.25 )



if __name__ == '__main__':
    main()
